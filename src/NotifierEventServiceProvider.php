<?php

namespace Webmagic\Notifier;


use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Support\Facades\Event;
use Webmagic\Notifier\Listeners\NotifierListenerInterface;

class NotifierEventServiceProvider extends EventServiceProvider
{
    /**
     * NotifierEventServiceProvider constructor.
     *
     * @internal param array $listen
     */
    public function __construct($app)
    {
        parent::__construct($app);

    }

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $events = config('webmagic.notifier.events');

        foreach ($events as $event)
        {
            Event::listen($event, NotifierListenerInterface::class);
        }
    }
}
