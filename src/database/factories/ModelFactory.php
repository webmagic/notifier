<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * Notifications
 */
$factory->define(\Webmagic\Notifier\Notification\Notification::class, function (Faker\Generator $faker) {
    return [
        'event' => 'Webmagic\Request\Events\BaseEvent',
        'type' => 'mail',
        'data_id' => function () {
            return factory(\Webmagic\Notifier\Mail\Mail::class)->create()->id;
        }
    ];
});


/**
 * Notifications
 */
$factory->define(\Webmagic\Notifier\Mail\Mail::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});