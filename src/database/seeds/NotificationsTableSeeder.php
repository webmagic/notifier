<?php

use Illuminate\Database\Seeder;

class NotificationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('notifications')->delete();

        \DB::table('notifications')->insert(array (
            0 =>
            array (
                'id' => 1,
                'event' => 'Webmagic\\Log\\Events\\LogEvent',
                'type' => 'mail',
                'data_id' => '1',
                'created_at' => time(),
                'updated_at' => time(),
            )
        ));


    }
}
