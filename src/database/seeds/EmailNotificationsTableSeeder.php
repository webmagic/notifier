<?php

use Illuminate\Database\Seeder;

class EmailNotificationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('email_notifications')->delete();

        \DB::table('email_notifications')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Log notifications',
                'address' => '',
                'subject' => "Log information",
                'subject_type' => 'view',
                'body' => '',
                'template' => 'notifier::mail.log',
                'created_at' => time(),
                'updated_at' => time(),
                'files' => NULL,
            )
        ));


    }
}
