<?php


namespace Webmagic\Notifier\Mail;


use Webmagic\Core\Entity\EntityRepo;

class MailRepo extends EntityRepo
{

    /** @var  Model */
    protected $entity = Mail::class;
}