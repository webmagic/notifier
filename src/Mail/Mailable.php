<?php


namespace Webmagic\Notifier\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable as BaseMailable;
use Illuminate\Queue\SerializesModels;

class Mailable extends BaseMailable implements ShouldQueue
{
    use Queueable, SerializesModels;


    /** @var string template name */
    protected $template_path;

    /** @var string body of email */
    protected $body;

    /** @var string type must be view or markdown */
    protected $type;

    /** @var array of data from event */
    protected $event_data;

    /** @var string addresses */
    protected $address;

    /** @var mixed  files */
    protected $files;

    /**
     * Mailable constructor.
     *
     * @param string $template_path
     * @param string $address
     * @param string $subject
     * @param string $body
     * @param string $type
     * @param array  $event_data
     */
    public function __construct(string $template_path, string $address, string $subject, string $body, string $type, $files ,array $event_data)
    {
        $this->template_path = $template_path;
        $this->address = $address;
        $this->subject = $subject;
        $this->body = $body;
        $this->type = $type;
        $this->files = $files;
        $this->event_data = $event_data;
    }


    /**
     * Build the message.
     *(
     * @return $this
     */
    public function build()
    {
        $body = $this->prepareData($this->body);
        $subject = $this->prepareData($this->subject);
        $addresses = $this->prepareData($this->address);
        $emails_list = explode(',', $addresses);

         $this->subject($subject)
            ->with([
                'body' => $body,
                'data' => $this->event_data
            ])
            ->{$this->type}($this->template_path);


            if($this->files) {
                foreach ($this->files as $pathToFile) {
                    $this->subject($subject)->attach($pathToFile);
                }
            }

        //todo Prepare better realization for prevent sending email when address incorrect
        return $emails_list[0] ? $this->to($emails_list) : $this;
    }


    /**
     * Set variables in template
     *
     * @param       $content
     *
     * @return mixed
     */
    protected function prepareData($content)
    {
        preg_match_all('~\{{.*?\}}~', $content, $params_keys);

        foreach ($params_keys[0] as $param) {
            $clear_param = trim(str_replace(['{{', '}}'], '', $param));
            $val = $this->getParamVal($clear_param);
            $content = str_replace($param, $val, $content);
        }

        return $content;
    }


    /**
     * Prepare param
     *
     * @param $param
     *
     * @return mixed|null
     */
    protected function getParamVal($param)
    {
        $data = $this->event_data;

        if (isset($data[$param])) {
            return is_array($data[$param]) ? 'array' : $data[$param];
        }

        return null;
    }
}
