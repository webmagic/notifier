<?php


namespace Webmagic\Notifier\Mail;


use Webmagic\Notifier\Entity\Notifier;
use Illuminate\Support\Facades\Mail as BaseMail;
use Webmagic\Notifier\NotifierService;

class Mail extends Notifier
{
    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Available for
     */
    protected $fillable = ['name', 'address', 'subject', 'subject_type', 'body', 'template', 'files'];

    /**
     * Name of table in db
     */
    protected $table = 'email_notifications';


    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.notifier.mails_additional_fields'));

        parent::__construct($attributes);
    }


    /**
     * Path to sending template
     *
     * @return string
     */
    public function getPathToTemplate()
    {
        if (view()->exists($this->template)) {
            return $this->template;
        }

        $notifierService =  app()->make(NotifierService::class);
        return $notifierService->getDefaultTemplate();
    }


    /**
     * Path to for form for creating notification
     *
     * @return string
     */
    public function getPathToForm()
    {
        return 'notifier::mail._form';
    }


    /**
     * Sending email
     * @param $event_data
     *
     * @return mixed|void
     */
    public function send($event_data, $files)
    {
        if(config('webmagic.notifier.mail_queue_use')){
            BaseMail::queue(new Mailable($this->getPathToTemplate(), $this->address, $this->subject, $this->body, $this->subject_type, $files, $event_data));
        } else{
            BaseMail::send(new Mailable($this->getPathToTemplate(), $this->address, $this->subject, $this->body, $this->subject_type, $files, $event_data));
        }
    }




}