<?php

namespace Webmagic\Notifier;

use Illuminate\Routing\Router;
use Webmagic\Dashboard\Dashboard;

class NotifierDashboardServiceProvider extends NotifierServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        //Routs registering
        include 'routes/dashboard.php';

        //Config publishing
        $this->publishes([
            __DIR__ . '/config/notifier_dashboard.php' => config_path('webmagic/dashboard/notifier.php')
        ], 'notifier-dashboard-config');

        //Publish views
        $this->publishes([
            __DIR__ . '/resources/views/' => base_path('resources/views/vendor/webmagic/notifier'),
        ], 'notifier-views');

        //Publish views
        $this->publishes([
            __DIR__ . '/resources/lang/' => resource_path('lang/vendor/notifier'),
        ], 'lang');

        $this->loadTranslationsFrom(__DIR__ . '/resources/lang/', 'notifier');

        $this->loadDashboardMenuItem();
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__ . '/config/notifier_dashboard.php', 'webmagic.dashboard.notifier'
        );
    }

    /**
     * Load items for menu
     */
    protected function loadDashboardMenuItem()
    {
        //Add dashboard menu item
        $menu_item_config = config('webmagic.dashboard.notifier.dashboard_menu_item');

        if(!$menu_item_config) {
            return;
        }
        
        //Add menu into global menu
        $dashboard = $this->app->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }
}
