@foreach($event_fields as $key => $field)
    @if(!is_array($field))
        <li>{{ $field }}</li>
    @else
        <li>
            <b>{{ $key }}[]</b>
            <br><i>(for template only)</i> [
            <ul>
                @foreach($field as $fieldKey => $fieldName)
                    <li>@if(!is_array($fieldName)){{$fieldName}} @else {{$fieldKey}}@endif</li>
                @endforeach
            </ul>
            ]
        </li>
    @endif
@endforeach

<br>
<p><b>@lang('notifier::common.email-variable-example'): @{{name}}</b>
</p>