<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for notifier
    |--------------------------------------------------------------------------
    |
    */
    'notifications' => 'Уведомления',
    'add' => 'Доабвить',
    'id' => 'ID',
    'type' => 'Тип',
    'event' => 'Событие',
    'delete' => 'Удалить',
    'nothing-found' => 'Ничего не найдено',
    'notification-creation' => 'Создание уведомления',
    'save' => 'Сохранить',
    'some-of-events-unavailable' => 'Подключены события, не реализующие требуемый интерфейс',
    'email' => 'Email',
    'available-variables' => 'Доступные переменные',
    'email-variable-example' => 'Для использования переменных необходимо указать имя в фигурных скобках. Пример',
    'email-template' => 'Шаблон письма',
    'email-body-input' => 'Содержание (возможно использование переменных)',
    'email-title-input' => 'Тема письма (возможно использование переменных)',
    'email-addresses-input' => 'Адресаты (возможно использование переменных)',
    'email-name' => 'Название',
    'attach_file' => 'Прикрепить файл'
];