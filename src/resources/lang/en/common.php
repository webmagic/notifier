<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for notifier
    |--------------------------------------------------------------------------
    |
    */
    'notifications' => 'Notifications',
    'add' => 'Add',
    'id' => 'ID',
    'type' => 'Type',
    'event' => 'Event',
    'delete' => 'Delete',
    'nothing-found' => 'Nothing found',
    'notification-creation' => 'Notification creation',
    'save' => 'Save',
    'some-of-events-unavailable' => 'Some events do not implement the interface',
    'email' => 'Email',
    'available-variables' => 'Available variables',
    'email-variable-example' => 'To use variables, you must specify a name in curly brackets. Example',
    'email-template' => 'Email template',
    'email-body-input' => 'Content (variables available)',
    'email-title-input' => 'Subject (variables available)',
    'email-addresses-input' => 'Email addresses (variables available)',
    'email-name' => 'Name',
    'attach_file' => 'Attach files'
];