<?php
Route::group([
    'prefix' => config('webmagic.dashboard.notifier.prefix'),
    'namespace' => '\Webmagic\Notifier\Http\Controllers',
    'middleware' => config('webmagic.dashboard.notifier.middleware')],
    function () {

        Route::group([
            'as' => 'notifier::',
        ], function () {

            Route::resource('notification', 'DashboardController',
                ['except' => ['show'],
                    'names' => [
                        'index' => 'index',
                        'create' => 'create',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy'
                    ]]
            );

        });
    });