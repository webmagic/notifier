<?php


namespace Webmagic\Notifier\Notification;


use Illuminate\Database\Eloquent\Builder;
use Webmagic\Core\Entity\EntityRepo;

class NotificationRepo extends EntityRepo
{

    /** @var  Model */
    protected $entity = Notification::class;


    /**
     * Return all notifications by event name
     *
     * @param string $event
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     */
    public function getAllByEventName(string $event)
    {
        $query = $this->query();
        $query->where('event', $event);

        return $this->realGetMany($query);
    }
}