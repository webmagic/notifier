<?php


namespace Webmagic\Notifier\Notification;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Notifier\Mail\Mail;

class Notification extends Model
{
    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * Available for
     */
    protected $fillable = ['event', 'type', 'data_id'];

    /**
     * Name of table in db
     */
    protected $table = 'notifications';


    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.notifier.notifications_additional_fields'));

        parent::__construct($attributes);
    }


    /**
     * Notification relations with different types
     *
     * @param $type_class
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function type($type_class)
    {
        return $this->belongsTo($type_class, 'data_id');
    }





}