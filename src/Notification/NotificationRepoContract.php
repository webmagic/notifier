<?php


namespace Webmagic\Notifier\Notification;


use Illuminate\Support\Collection;
use Webmagic\Core\Entity\EntityRepoInterface;

interface NotificationRepoContract extends EntityRepoInterface
{
    /**
     * Return all notifications by event name
     *
     * @param string $event_class
     *
     * @return Collection
     */
    public function getAllByEventName(string $event_class): Collection;
}