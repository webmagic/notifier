<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Events list
    |--------------------------------------------------------------------------
    |
    | Registration events and association mails listeners for this events
    |
    */

    'events' => [
        'Webmagic\Request\Events\BaseEvent'
    ],

    /*
    |--------------------------------------------------------------------------
    | Email template
    |--------------------------------------------------------------------------
    |
    | On/Off using templates for emails
    | Defining list of available templates
    |
    */

    'use_email_templates' => false,
    'email_templates' => [
        'default' => 'notifier::mail.template'
    ],


    /*
    |--------------------------------------------------------------------------
    | Types list
    |--------------------------------------------------------------------------
    |
    | Registration types alias and association with their class
    |
    */

    'types' => [
        'mail' => 'Webmagic\Notifier\Mail\Mail',
    ],


    /*
    |--------------------------------------------------------------------------
    | Using queue in sending mail
    |--------------------------------------------------------------------------
    */
    'mail_queue_use' => false,


    /*
    |--------------------------------------------------------------------------
    | Additional fields for notifications and different types
    |--------------------------------------------------------------------------
    */
    'notifications_additional_fields' => [],

    /*
     * Types
     */
    'mails_additional_fields' => [],

    /*
    |--------------------------------------------------------------------------
    | Additional templates
    |--------------------------------------------------------------------------
    |
    | Array with additional templates which will be available for notify by email
    | All variables will be available in selected template
    |
    */

    'additional_templates' => [

    ]

];
