<?php

namespace Webmagic\Notifier;


use Exception;
use Illuminate\View\View;
use Webmagic\Notifier\Contracts\Notifiable;
use Webmagic\Notifier\Notification\NotificationRepo;

class NotifierService extends NotificationRepo
{

    /**
     * Find all events from config
     *
     * @return mixed
     */
    public function findEvents()
    {
        $events  = config('webmagic.notifier.events');

        $array['implementation'] = [];
        $array['not_implementation'] = [];

        foreach($events as $event)
        {
            if($this->checkImplementation($event)){
                $array['implementation'][$event] = $event;
            } else{
                $array['not_implementation'][] = $event;
            }
        }

        return $array;
    }


    /**
     * Check of existing event class and implementation special interface
     *
     * @param $event
     * @return bool
     * @throws Exception
     */
    protected function checkImplementation($event)
    {
        if(!class_exists($event)){
            throw new Exception("Please check path to your event class in the notifier config: $event");
        }

        $interfaces = class_implements($event);
        return key_exists(Notifiable::class, $interfaces);
    }


    /**
     * Get from config all types
     *
     * @return array
     */
    public function getAllTypes()
    {
        return config('webmagic.notifier.types');
    }


    /**
     * Create notification and empty entity of type
     *
     * @param array $request_data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $request_data)
    {
        $type = $this->getTypeClassByAlias($request_data['type'])->create([]);

        $request_data['data_id'] = $type->id;
        return parent::create($request_data);
    }


    /**
     * Make entity of type by alias
     *
     * @param string $type_name
     * @return mixed
     */
    protected function getTypeClassByAlias(string $type_name)
    {
        $type_name = $this->getAllTypes()[$type_name];
        return app()->make($type_name);
    }


    /**
     * Updating template(notification by type)
     *
     * @param string $type_name
     * @param $type_id
     * @param $type_data
     * @return mixed
     */
    public function updateTypeTemplateById(string $type_name, $type_id, array $type_data)
    {
        $type = $this->getTypeClassByAlias($type_name);

        if(isset($type_data['files']) && count($type_data['files'] > 1)){
            $type_data['files'] = implode(',', $type_data['files']);
        }else{
            $type_data['files'] = null;
        }

        return $type->update($type_id, $type_data);
    }


    /**
     * Get required fields from event
     *
     * @param string $event_class
     * @return array
     * @throws Exception
     */
    public function getEventFields(string $event_class)
    {
        $this->checkImplementation($event_class);

        if(!$this->checkImplementation($event_class)) {

            throw new Exception("Chosen event doesn't implement interface" . Notifiable::class );
        }

        return $event_class::getAvailableFields();
    }


    /**
     * Get required fields from event
     *
     * @param string $event_class
     * @return array
     * @throws Exception
     */
    public function getEventFiles(string $event_class)
    {
        $this->checkImplementation($event_class);

        if(!$this->checkImplementation($event_class)) {

            throw new Exception("Chosen event doesn't implement interface" . Notifiable::class );
        }

        return $event_class::getAvailableFiles();
    }

    /**
     * Return default template
     *
     * @return mixed|string
     */
    public function getDefaultTemplate()
    {
        $templates = $this->getAvailableEmailTemplates();

        if(isset($templates['default'])) {
            return $templates['default'];
        }

        return 'notifier::mail.template';
    }

    /**
     * Return available templates
     *
     * @return array
     */
    public function getAvailableEmailTemplates(): array
    {
        $templates = config('webmagic.notifier.email_templates');

        //Check templates availability
        foreach ($templates as $key => $template) {
            if (!view()->exists($template)) {
                unset($templates[$key]);
            }
        }

        return $templates;
    }

    /**
     *  Check if templates available
     *
     * @return bool
     */
    public function isEmailTemplatesAvailable(): bool
    {
        return (bool)config('webmagic.notifier.use_email_templates');
    }
}