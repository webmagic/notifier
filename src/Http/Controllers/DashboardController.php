<?php


namespace Webmagic\Notifier\Http\Controllers;


use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Webmagic\Dashboard\Components\FormGenerator;
use Webmagic\Dashboard\Components\TablePageGenerator;
use Webmagic\Dashboard\Dashboard;
use Webmagic\Dashboard\Elements\Forms\Form;
use Webmagic\Notifier\NotifierService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * DashboardController constructor.
     * @param Dashboard $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }


    /**
     * Show list of all notifications
     *
     * @param NotifierService $notifierService
     * @return Dashboard
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function index(NotifierService $notifierService)
    {
        $notifications = $notifierService->getAll(10);

        (new TablePageGenerator($this->dashboard->page()))
            ->title('Clients list')
            ->tableTitles([__('notifier::common.id'), __('notifier::common.type'), __('notifier::common.event'), ''])
            ->showOnly(['id', 'type', 'event'])
            ->items($notifications)
            ->withPagination($notifications, route('notifier::index'))
            ->setEditLinkClosure(function ($notification){
                return route('notifier::edit', $notification['id']);
            })
            ->setDestroyLinkClosure(function ($notification){
                return route('notifier::destroy', $notification['id']);
            })
            ->createLink(route('notifier::create'));

        // For pagination
        if(request()->ajax()){
            return $this->dashboard->page()->content()->toArray()['box_body'];
        }

        return $this->dashboard;
    }


    /**
     * Show form for creating notification (check type and event)
     *
     * @param NotifierService $notifierService
     * @return Dashboard
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function create(NotifierService $notifierService)
    {
        $events = $notifierService->findEvents();
        $types = $notifierService->getAllTypes();

        $formGenerator = (new FormGenerator())
            ->action(route('notifier::store'))
            ->method('POST')
            ->ajax(true)
            ->select('type', $types, null, __('notifier::common.type'))
            ->select('event', $events['implementation'], null, __('notifier::common.event'), true)
            ->submitButton(__('notifier::common.save'))
        ;

        $this->dashboard->page()->setPageTitle(__('notifier::common.notification-creation'))
            ->addElement()->box()
            ->content($formGenerator->render())
        ;

        if(count($events['not_implementation']) > 0) {
            $this->dashboard->page()
                ->addElement()->box()->boxTitle(__('notifier::common.some-of-events-unavailable') . ' <b>Webmagic\Notifier\Contracts\Notifiable</b>')
                ->element()->descriptionList(['data' => $events['not_implementation']]);
        }

        return $this->dashboard;
    }


    /**
     * Creating notification
     *
     * @param Request $request
     * @param NotifierService $notifierService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, NotifierService $notifierService)
    {
        if(!$notifierService->create($request->all())){
            return response('Error on create', 500);
        }
    }

    /**
     * Show form for edit notification
     *
     * @param $notification_id
     * @param NotifierService $notifierService
     * @return Dashboard
     * @throws \Webmagic\Dashboard\Core\Content\Exceptions\NoOneFieldsWereDefined
     */
    public function edit($notification_id, NotifierService $notifierService)
    {
        if(!$notification = $notifierService->getByID($notification_id)){
            abort(404);
        }

        $types = $notifierService->getAllTypes();
        $events = $notifierService->findEvents();

        $isTemplatesAvailable = $notifierService->isEmailTemplatesAvailable();
        if($isTemplatesAvailable) {
            $templates = $notifierService->getAvailableEmailTemplates();
            $templates = array_flip($templates);
        } else {
            $templates['default'] = $notifierService->getDefaultTemplate();
        }

        //Get type by relations with notifications
        $type_class = $types[$notification['type']];
        $type = $notification->type($type_class)->first();

        //Get array of fields from special method in event class getFieldsData()
        $event_fields = $notifierService->getEventFields($notification['event']);
        $event_files = $notifierService->getEventFiles($notification['event']);

        // get selected files and explode them for multi select
        $event_selected_files = explode(',', $type['files']);

        $form = (new FormGenerator())
            ->action(route('notifier::update', $notification['id']))
            ->method('PUT')
            ->ajax(true)
            ->textInput('name', $type['name'], __('notifier::common.email-name'))
            ->textarea('address', $type['address'], __('notifier::common.email-addresses-input'))
            ->textInput('subject', $type['subject'], __('notifier::common.email-title-input'))
            ->visualEditor('body', $type['body'], __('notifier::common.email-body-input'))
        ;

        $form->getForm()->addElement()->input()->name('subject_type')->type('hidden')->value('view');

        $form->getForm()->addElement()->box()->boxTitle(__('notifier::common.available-variables'))
            ->content((view('notifier::mail._part', compact('event_fields'))));
        ;

        if($isTemplatesAvailable){
            $form->getForm()->addElement()
                ->formGroup()->labelTxt(__('notifier::common.email-template'))->element()
                ->select()->name('template')->options($templates)->selectedKey($type['template']);
        }else{
            $form->getForm()->addElement()->input()->name('template')->type('hidden')->value($type->template ?:$templates['default']);
        }

        $form->getForm()->addElement()
            ->formGroup()->labelTxt(__('notifier::common.event'))->element()
            ->select()->name('event')->options($events['implementation'])->selectedKey($notification['event']);

        $form->getForm()->addElement()
            ->formGroup()->labelTxt(__('notifier::common.attach_file'))->element()
            ->selectJS()->name('files[]')->options($event_files)->selectedKeys($event_selected_files)->multiple(true);

        $form->getForm()->addElement()->input()->name('notification_id')->type('hidden')->value($notification->id);

        if(count($events['not_implementation']) > 0) {
            $form->getForm()->addElement()
                ->stringElement('<p style="color: red">'. __('notifier::common.some-of-events-unavailable') . ' <b>Webmagic\Notifier\Contracts\Notifiable</b><p/>');

            $form->getForm()->addElement()->descriptionList(['data' => $events['implementation']]);
        }

        $form->getForm()->addElement()->button(__('notifier::common.save'))->type('submit');

        $this->dashboard->page()->setPageTitle(__('notifier::common.email'))
            ->addElement()->box()
            ->content($form->render())
        ;

        return $this->dashboard;
    }


    /**
     * Updating notification
     *
     * @param $notification_id
     * @param Request $request
     * @param NotifierService $notifierService
     * @throws \Exception
     */
    public function update($notification_id, Request $request, NotifierService $notifierService)
    {
        if(!$notification = $notifierService->getByID($notification_id)){
            abort(404);
        }

        $notifierService->updateTypeTemplateById($notification['type'], $notification['data_id'],$request->all());

        $notifierService->update($request->notification_id, ['event' => $request->event]);
    }

    /**
     * Destroy notification by id
     *
     * @param $notification_id
     * @param NotifierService $notifierService
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Webmagic\Core\Entity\Exceptions\EntityNotExtendsModelException
     * @throws \Webmagic\Core\Entity\Exceptions\ModelNotDefinedException
     */
    public function destroy($notification_id, NotifierService $notifierService)
    {
        if(!$notifierService->destroy($notification_id)){
            return response('Error on delete', 500);
        }
    }
}