<?php


namespace Webmagic\Notifier\Contracts;


interface Notifiable
{


    /**
     * Return array of available fields for hints for user in admin panel
     *
     * @return array
     */
    public static function getAvailableFields(): array;


    /**
     * Return array of data for notifications where keys ara field's name
     *
     * @return array
     */
    public function getFieldsData(): array;

    /**
     * Return array with files
     *
     * @return array
     */
    public static function getAvailableFiles(): array;
}
