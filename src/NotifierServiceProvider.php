<?php

namespace Webmagic\Notifier;


use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Webmagic\Notifier\Listeners\NotifierListener;
use Webmagic\Notifier\Listeners\NotifierListenerInterface;
use Webmagic\Notifier\Mail\Mail;
use Webmagic\Notifier\Mail\MailRepo;
use Webmagic\Notifier\Notification\NotificationRepo;
use Webmagic\Notifier\Notification\NotificationRepoContract;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factory;

class NotifierServiceProvider extends ServiceProvider
{
    /**
     * Register services
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/notifier.php', 'webmagic.notifier'
        );

        $this->registerProviders();
        $this->registerFactories();
    }


    /**
     * Bootstrapping services
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        //Config publishing
        $this->publishes([
            __DIR__ . '/config/notifier.php' => config_path('webmagic/notifier.php')
        ], 'notifier-config');

        //Migrations publishing
        $this->publishes([
            __DIR__ . '/database/migrations/' => database_path('/migrations'),
        ], 'notifier-migrations');

        //Seeds publishing
        $this->publishes([
            __DIR__ . '/database/seeds/' => database_path('/seeds'),
        ], 'notifier-seeds');


        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'notifier');

        $this->registeringMiddleware($router);

        $this->loadMigrations();
    }

    /**
     * Register providers
     */
    protected function registerProviders()
    {
        $this->app->bind(NotifierListenerInterface::class, NotifierListener::class);

        $this->app->bind(Mail::class, MailRepo::class);
        $this->app->bind(NotificationRepoContract::class, NotificationRepo::class);

        $this->app->register('\Webmagic\Notifier\NotifierEventServiceProvider');
    }

    /**
     * Middleware registration
     *
     * @param $router
     */
    protected function registeringMiddleware($router)
    {
        $router->middlewareGroup('notifier', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]);
    }


    /**
     * Factories registration
     */
    protected function registerFactories()
    {
        $this->app->singleton(Factory::class, function ($app){
            return Factory::construct($app->make(Generator::class),  __DIR__.'/database/factories/');
        });
    }

    /**
     * Load migrations
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}