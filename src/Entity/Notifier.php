<?php


namespace Webmagic\Notifier\Entity;


use Illuminate\Database\Eloquent\Model;

abstract class Notifier extends Model
{


    /**
     * Sending notify
     *
     * @param $event_data
     *
     * @param $files
     * @return mixed
     */
    abstract public function send($event_data, $files);


    /**
     * Return path to view for creating notification message
     *
     * @return string
     */
    abstract public function getPathToForm();

}