<?php

namespace Webmagic\Notifier\Listeners;


use Webmagic\Notifier\Contracts\Notifiable;
use Webmagic\Notifier\NotifierService;

class NotifierListener implements NotifierListenerInterface
{
    /**
     * Mailer service
     *
     * @var \Webmagic\Notifier\NotifierService
     */
    protected $notifierService;

    /**
     * Create the event listener.
     *
     * @param NotifierService $notifierService
     */
    public function __construct(NotifierService $notifierService)
    {
        $this->notifierService = $notifierService;
    }

    /**
     * Handle the event.
     *
     * @param $event
     */
    public function handle(Notifiable $event)
    {
        $data = $event->getFieldsData();
        $files = $event->getAvailableFiles();
        $event_class = get_class($event);
        $notifications = $this->notifierService->getAllByEventName($event_class);
        $types = $this->notifierService->getAllTypes();

        foreach($notifications as $notification)
        {
            $type_class = $types[$notification['type']];
            $type = $notification->type($type_class)->first();
            $preparedFiles = $this->prepareFile($type, $files, $data);

            $type->send($data, $preparedFiles);
        }

    }


    protected function prepareFile($type, $files, $data)
    {
        $preparedFiles = [];
        $availableFiles = explode(',', $type['files']);

        foreach ($files as $id => $fileName){
            // check if file attached and file exist
            if(empty($type['files']) || !in_array($id, $availableFiles) || empty($data['request.'.$fileName])){
                continue;
            }

            $preparedFiles[] = $data['request.'.$fileName];
        }
        return $preparedFiles;
    }
}
