<?php

namespace Tests\Unit\Pages;

use Tests\TestCase;
use Webmagic\Notifier\Notification\Notification;
use Webmagic\Users\Models\User;

class PagesTest extends TestCase
{
    /**
     * RequestType
     */
    //Page for notifications list
    public function testIndex()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('notifier::index'));
        $response->assertStatus(200);
    }

    //Page for notification creating
    public function testCreate()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('notifier::create'));
        $response->assertStatus(200);
    }

    //Page for notification editing
    public function testEdit()
    {
        $user = new User();
        $notification = factory(Notification::class)->create();

        $response = $this->actingAs($user)->get(route('notifier::edit', $notification->id));
        $response->assertStatus(200);
    }
}
