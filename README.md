### QUICK START FOR SET UP WITH DASHBOARD

For publishing run next commands:
 #### dashboard config 
    vendor:publish --tag=notifier-dashboard-config
 
 #### views 
    vendor:publish --tag=notifier-views
  
  #### migrations 
    vendor:publish --tag=notifier-migrations
    
 ####  seeds
    vendor:publish --tag=notifier-seeds
    
 ####  config
    vendor:publish --tag=notifier-config
    
Then include NotifierDashboardServiceProvider in config/app.php in providers

```php
'providers' => [
    ....
    \Webmagic\Notifier\NotifierDashboardServiceProvider::class
]
```
  